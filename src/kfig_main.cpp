/*
 * SPDX-FileCopyrightText: 2021 Weixuan XIAO <veyx.shaw@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include <QApplication>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    return app.exec();
}


